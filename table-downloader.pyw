import pathlib
import tkinter as tk
import logging
from collections import deque
from tkinter import filedialog, messagebox
from typing import List, Deque, Union
import pymysql.cursors
import csv
import configparser
import os

IS_ADMIN = False

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


class Table:
    def __init__(self, name) -> None:
        self.name = name


class DB:
    def __init__(self, name: str, path, tables: Deque[Table]) -> None:
        self.name: str = name
        self.path: str = path
        self.tables: Deque[Table] = tables


class Config:
    MYSQL_CREDENTIALS_SECTION_NAME = '^mysql_credentials'

    def __init__(self):
        current_dir_path = pathlib.Path(__file__).parent.absolute()
        self.config_file_path = config_path = current_dir_path / 'config.ini'
        self.db_buttons: List[DB] = []
        self.config = self._read_config_file(config_path)

    @property
    def host(self) -> str:
        return self.config.get(self.MYSQL_CREDENTIALS_SECTION_NAME, 'host')
    @host.setter
    def host(self, host: str):
        self.config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'host', host)

    @property
    def port(self) -> int:
        return self.config.getint(self.MYSQL_CREDENTIALS_SECTION_NAME, 'port')
    @port.setter
    def port(self, port: str):
        self.config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'port', port)

    @property
    def username(self) -> str:
        return self.config.get(self.MYSQL_CREDENTIALS_SECTION_NAME, 'username')
    @username.setter
    def username(self, username: str):
        self.config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'username', username)

    @property
    def password(self) -> str:
        return self.config.get(self.MYSQL_CREDENTIALS_SECTION_NAME, 'password')
    @password.setter
    def password(self, password: str):
        self.config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'password', password)

    @property
    def database(self) -> str:
        return self.config.get(self.MYSQL_CREDENTIALS_SECTION_NAME, 'database')
    @database.setter
    def database(self, db: str):
        self.config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'database', db)

    def save_config(self, config: configparser.ConfigParser = None):
        for db in self.db_buttons:
            if not self.config.has_section(db.name):
                self.config.add_section(db.name)
            self.config.set(db.name, 'tables', ','.join([t.name for t in db.tables]))
            self.config.set(db.name, 'path', db.path)
        with open(self.config_file_path, 'w') as f:
            if config:
                config.write(f)
            else:
                self.config.write(f)

    def _read_config_file(self, path: pathlib.Path) -> configparser.ConfigParser:
        if not path.is_file():
            return self._create_default_config()

        config = configparser.ConfigParser()
        config.read_file(open(path))
        for section in config.sections():
            if section == self.MYSQL_CREDENTIALS_SECTION_NAME:
                continue

            tables = deque()
            for table_name in config.get(section, 'tables').split(','):
                tables.append(Table(table_name))

            path = config.get(section, 'path')
            db = DB(section, path, tables)
            self.db_buttons.append(db)
        return config

    def _create_default_config(self) -> configparser.ConfigParser:
        config = configparser.ConfigParser()
        config.add_section(self.MYSQL_CREDENTIALS_SECTION_NAME)
        config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'host', '127.0.0.1')
        config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'port', '3306')
        config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'username', 'root')
        config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'password', 'password')
        config.set(self.MYSQL_CREDENTIALS_SECTION_NAME, 'database', 'test')
        self.save_config(config)
        return config


class Application(tk.Tk):
    window_bg_color = '#faf3dd'

    def __init__(self, config: Config):
        super().__init__()
        self.cfg = config
        # configure main window
        self.geometry('800x600')
        self.configure(background=self.window_bg_color)
        self.title('PyDB downloader')
        self._center_window(self)
        # main menu
        self.main_menu = main_menu = tk.Menu(self)
        main_menu.add_command(label='Configure', command=self.open_config_window)
        if IS_ADMIN:
            self.config(menu=main_menu)
        # configure main main frame


        self.main_container = tk.Frame(self)

        self.main_frame_canvas = tk.Canvas(self.main_container, borderwidth=0, background=self.window_bg_color)
        self.main_frame = tk.Frame(self.main_frame_canvas, bg=self.window_bg_color)
        self.main_frame_vsb = tk.Scrollbar(self.main_container,                                           orient='vertical',
                                           command=self._scroll_frame(self.main_frame, self.main_frame_canvas)
                                           )

        self.main_frame.bind(
            '<Configure>',
            lambda e: self.main_frame_canvas.configure(scrollregion=self.main_frame_canvas.bbox('all'))
        )


        main_frame_id = self.main_frame_canvas.create_window((0,0), window=self.main_frame, anchor='nw')

        self.main_frame_canvas.configure(yscrollcommand=self.main_frame_vsb.set)
        self.main_frame_canvas.bind('<Enter>', self._bound_to_mousewheel(self.main_frame, self.main_frame_canvas))
        self.main_frame_canvas.bind('<Leave>', self._unbound_to_mousewheel(self.main_frame_canvas))
        self.main_frame_canvas.configure(width=0)

        self.main_container.pack(side=tk.LEFT, fill='both', expand=True)
        self.main_frame_canvas.pack(side=tk.LEFT, fill='both', expand=True)
        self.main_frame_vsb.pack(side=tk.RIGHT, fill='y')

        # configure options container
        self.options_container = tk.LabelFrame(self, text='Options')

        self.options_frame_canvas = tk.Canvas(self.options_container, borderwidth=0, background=self.window_bg_color)
        self.options_frame = tk.Frame(self.options_frame_canvas, bg=self.window_bg_color)
        self.options_frame_vsb = tk.Scrollbar(self.options_container,
                                              orient='vertical',
                                              command=self._scroll_frame(self.options_frame, self.options_frame_canvas)
                                              )

        self.options_frame.bind(
            '<Configure>',
            lambda e: self.options_frame_canvas.configure(scrollregion=self.options_frame_canvas.bbox('all'))
        )

        self.options_frame_canvas.create_window((0, 0), window=self.options_frame, anchor='nw')
        self.options_frame_canvas.configure(yscrollcommand=self.options_frame_vsb.set)

        self.options_frame_canvas.bind('<Enter>', self._bound_to_mousewheel(self.options_frame,self.options_frame_canvas))
        self.options_frame_canvas.bind('<Leave>', self._unbound_to_mousewheel(self.options_frame_canvas))
        self.options_frame_canvas.configure(width=0)
        self.options_container.pack(side=tk.RIGHT, fill='both', expand=True)
        self.options_frame_canvas.pack(side=tk.LEFT, fill='both', expand=True)
        self.options_frame_vsb.pack(side=tk.RIGHT, fill='y')

        self.create_db_buttons()

    def open_config_window(self):
        window = tk.Toplevel()
        window.geometry('400x200')
        cfg_frame = tk.Frame(window)
        cfg_frame.pack(side=tk.TOP, fill='both', expand=True)
        lbl = tk.Label(cfg_frame, text='Label')
        lbl.pack(side=tk.TOP)

        db_credentials = tk.Frame(cfg_frame)
        db_credentials.pack(side=tk.TOP, anchor='center', fill='both', expand=True)
        # db_credentials.grid_rowconfigure(0, weight=1)
        # db_credentials.grid_columnconfigure(0, weight=1)

        host_label = tk.Label(db_credentials, text='Host:')
        host_label.grid(row=0, column=0, sticky='w')
        host_entry = tk.Entry(db_credentials)
        host_entry.insert(tk.END, self.cfg.host)
        host_entry.grid(row=0, column=1, sticky='w')

        port_label = tk.Label(db_credentials, text='Port:')
        port_label.grid(row=0, column=2, sticky='w')
        port_entry = tk.Entry(db_credentials)
        port_entry.config(width=10)
        port_entry.insert(tk.END, self.cfg.port)
        port_entry.grid(row=0, column=3, sticky='w')

        username_label = tk.Label(db_credentials, text='Username:')
        username_label.grid(row=1, column=0, sticky='w')
        username_entry = tk.Entry(db_credentials)
        username_entry.insert(tk.END, self.cfg.username)
        username_entry.grid(row=1, column=1, sticky='w')

        password_label = tk.Label(db_credentials, text='Password:')
        password_label.grid(row=2, column=0, sticky='w')
        password_entry = tk.Entry(db_credentials)
        password_entry.insert(tk.END, self.cfg.password)
        password_entry.grid(row=2, column=1, sticky='w')

        db_label = tk.Label(db_credentials, text='Database:')
        db_label.grid(row=3, column=0, sticky='w')
        db_entry = tk.Entry(db_credentials)
        db_entry.insert(tk.END, self.cfg.database)
        db_entry.grid(row=3, column=1, sticky='w')

        def save_config():
            self.cfg.host = host_entry.get()
            self.cfg.port = port_entry.get()
            self.cfg.username = username_entry.get()
            self.cfg.password = password_entry.get()
            self.cfg.database = db_entry.get()
            self.cfg.save_config()

        save_button = tk.Button(db_credentials, text='Save', command=save_config)
        save_button.configure(width=26)
        save_button.grid(row=4, column=0, columnspan=2)

        window.focus_set()
        window.grab_set()
        window.transient(self)
        window.wait_window(window)


    def _configure_interior(self, interior: tk.Frame, canvas: tk.Canvas):
        def f(e):
            # logger.debug(e)
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        return f


    def _configure_canvas(self, interior: tk.Frame, interior_id, canvas: tk.Canvas):
        def f (e):
            # logger.debug(e)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        return f

    def _scroll_frame(self, frame: tk.Frame, canvas: tk.Canvas):
        def f(*args):
            if canvas.winfo_height() <= frame.winfo_height():
                canvas.yview(*args)

        return f

    def _on_mousewheel(self, frame: tk.Frame, canvas: tk.Canvas):
        def f(event):
            s=-1

            if event.num == 5 or event.delta == -120:
                s =1
            if canvas.winfo_height() <= frame.winfo_height():
                canvas.yview_scroll(s, "units")
        return f

    def _bound_to_mousewheel(self, frame: tk.Frame, canvas: tk.Canvas):
        def f(e):
            global logger
            # logger.debug(e)
            canvas.bind_all('<MouseWheel>', self._on_mousewheel(frame, canvas))
            canvas.bind_all('<Button-4>', self._on_mousewheel(frame, canvas))
            canvas.bind_all('<Button-5>', self._on_mousewheel(frame, canvas))
        return f


    def _unbound_to_mousewheel(self, canvas: tk.Canvas):
        def f(e):
            # logger.debug(e)
            canvas.unbind_all('<MouseWheel>')
            canvas.unbind_all('<Button-4>')
            canvas.unbind_all('<Button-5>')
        return f

    def _center_window(self, wnd: tk.Tk):
        windowWidth = 640
        windowHeight = 480
        print("Width", windowWidth, "Height", windowHeight)

        # Gets both half the screen width/height and window width/height
        positionRight = int(wnd.winfo_screenwidth() / 2 - windowWidth / 2)
        positionDown = int(wnd.winfo_screenheight() / 2 - windowHeight / 2)

        # Positions the window in the center of the page.
        wnd.geometry("+{}+{}".format(positionRight, positionDown))

    def create_db_buttons(self):
        for i, db_btn in enumerate(self.cfg.db_buttons):
            if not IS_ADMIN:
                btn_cmd = self.download_tables(db_btn.tables, db_btn.path)
            else:
                btn_cmd = self.print_options(i)
            btn = tk.Button(self.main_frame, text=db_btn.name, command=btn_cmd)
            btn.pack(side=tk.TOP, anchor=tk.W)

    def _clear_widget(self, frame: tk.Frame):
        for widget in frame.winfo_children():
            widget.destroy()
        frame.pack_forget()

    def print_options(self, db_btn_index: int):
        def f():
            self._clear_widget(self.options_frame)
            output_dir_path_label = tk.Label(self.options_frame, text='Output directory:')
            output_dir_path_label.grid(row=0, column=0, sticky=tk.W)
            output_path = tk.StringVar()
            output_path.set(self.cfg.db_buttons[db_btn_index].path)
            output_dir_path = tk.Entry(self.options_frame,state='disabled', textvariable=output_path)
            output_dir_path.config(width=40)
            output_dir_path.grid(row=1, column=0, columnspan=4, sticky=tk.W)
            def open_path():
                d = filedialog.askdirectory()
                if not d:
                    return
                output_dir_path.configure(state='normal')
                output_dir_path.delete(0, tk.END)
                output_dir_path.insert(0, d)
                self.cfg.db_buttons[db_btn_index].path = d
                self.cfg.save_config()
                output_dir_path.configure(state='disabled')
                logger.debug(d)
            output_dir_path_change_button = tk.Button(self.options_frame, text='Change', command=open_path)
            output_dir_path_change_button.grid(row=2, column=0, sticky=tk.W)


            run_button = tk.Button(
                self.options_frame,
                text='Download',
                command=self.download_tables(self.cfg.db_buttons[db_btn_index].tables, output_path)

            )
            run_button.grid(row=3, column=0, sticky=tk.W)
            if IS_ADMIN:
                table_list_frame = tk.Frame(self.options_frame)
                table_list_frame.grid(row=4, column=0, sticky=tk.W)
                self._print_tables(table_list_frame, db_btn_index)

        return f

    def _print_tables(self, table_list_frame: tk.Frame, db_btn_index: int):
        self._clear_widget(table_list_frame)

        add_new_table_name = tk.Entry(table_list_frame)
        add_new_table_name.grid(row=0, column=0, sticky=tk.W)
        tables = self.cfg.db_buttons[db_btn_index].tables

        def add_table():
            name = add_new_table_name.get()
            tables.appendleft(Table(name))
            self.cfg.save_config()
            self._print_tables(table_list_frame, db_btn_index)

        add_new_table_button = tk.Button(table_list_frame, text='Add table', command=add_table)
        add_new_table_button.grid(row=0, column=1, sticky=tk.W, columnspan=2)

        for table_index, t in enumerate(tables):
            row = table_index + 1
            label = tk.Label(table_list_frame, text=t.name)
            label.grid(row=row, column=0, sticky=tk.W)

            def edit_table(table_index):
                def f():
                    row = table_index + 1
                    for column in range(3):
                        for s in table_list_frame.grid_slaves(row, column):
                            s.destroy()

                    edit_table_entry = tk.Entry(table_list_frame)
                    edit_table_entry.grid(row=row, column=0)

                    def save_table():
                        tables[table_index].name = edit_table_entry.get()
                        self.cfg.save_config()
                        self._print_tables(table_list_frame, db_btn_index)

                    edit_table_save_button = tk.Button(table_list_frame, text='Save', command=save_table)
                    edit_table_save_button.grid(row=row, column=1, columnspan=2, sticky=tk.W)

                return f

            edit_button = tk.Button(table_list_frame, text='Edit', command=edit_table(table_index))
            edit_button.grid(row=row, column=1, sticky=tk.W)

            def remove_table(i):
                def f():
                    del tables[i]
                    self.cfg.save_config()
                    self._print_tables(table_list_frame, db_btn_index)
                return f
            remove_button = tk.Button(table_list_frame, text='Remove', command=remove_table(table_index))
            remove_button.grid(row=row, column=2, sticky=tk.W)

    def open_file_dialog(self):
        e = filedialog.askopenfile()
        logger.debug(e)


    def download_tables(self, tables: Deque[Table], path: Union[tk.StringVar, str]):
        def f():
            nonlocal path
            try:
                connection = pymysql.connect(host=self.cfg.host,
                                             port=self.cfg.port,
                                             user=self.cfg.username,
                                             password=self.cfg.password,
                                             db=self.cfg.database,
                                             charset='utf8')
            except Exception as e:
                answ = tk.messagebox.showerror('Connection error', str(e))
                if answ == 'ok':
                   self.destroy()
                return
            try:
                for table in tables:

                        with connection.cursor() as cursor:
                            # Create a new record
                            sql = f'select * from {table.name}'
                            cursor.execute(sql)
                            if isinstance(path, tk.StringVar):
                                path = path.get()
                            with open(f'{path}/{table.name}.csv', 'w', newline='', encoding='utf-8') as f:
                                writer = csv.writer(f, delimiter=';')
                                desc = cursor.description
                                headers = [d[0] for d in desc]
                                writer.writerow(headers)
                                for row in cursor:
                                    writer.writerow(row)
            except Exception as e:
                tk.messagebox.showerror('Error', str(e).encode('utf-8', 'ignore'))
            else:
                tk.messagebox.showinfo(message='Alle Tabellen wurden erfolgreich heruntergeladen')

            finally:
                connection.close()
                self.destroy()

        return f


def main():
    app = Application(Config())
    app.mainloop()


if __name__ == '__main__':
    main()
